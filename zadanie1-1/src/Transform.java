import org.apache.bcel.generic.*;
import org.apache.bcel.Repository;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.Constants;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;

public class Transform {

    private static ClassGen classGen;
    private static ConstantPoolGen constantPoolGen;

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        JavaClass clazz = Repository.lookupClass(args[0]);
        classGen = new ClassGen(clazz);
        constantPoolGen = classGen.getConstantPool();

        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            prepareDesc(method);
        }

        classGen.getJavaClass().dump(Repository.lookupClassFile(classGen.getClassName()).getPath());
    }

    @SuppressWarnings("unchecked")
    private static void prepareDesc(Method method) {
        MethodGen methodGenerator = new MethodGen(method, classGen.getClassName(), constantPoolGen);
        InstructionFactory instructionFactory = new InstructionFactory(classGen);

        InstructionList instructionList = methodGenerator.getInstructionList();
        Iterator<InstructionHandle> it = instructionList.iterator();
        while (it.hasNext()) {
            InstructionHandle instructionHandle = it.next();

            if ((instructionHandle.getInstruction() instanceof InvokeInstruction)) {

                InvokeInstruction invokeInstruction = (InvokeInstruction) instructionHandle.getInstruction();
                Type returnType = invokeInstruction.getReturnType(constantPoolGen);

                if (!returnType.equals(Type.VOID)) {

                    String beforeCallInfo = "Method to be called: " + invokeInstruction.getMethodName(constantPoolGen) + invokeInstruction.getSignature(constantPoolGen);
                    instructionList.insert(instructionHandle, instructionFactory.createPrintln(beforeCallInfo));

                    if (!(invokeInstruction instanceof INVOKESPECIAL)) {
                        InstructionList returnInstr = new InstructionList();

                        returnInstr.append(instructionFactory.createFieldAccess(
                                System.class.getCanonicalName(), "out", new ObjectType(PrintStream.class.getCanonicalName()), Constants.GETSTATIC));

                        returnInstr.append(instructionFactory.createConstant("Got result: "));
                        returnInstr.append(instructionFactory.createInvoke(PrintStream.class.getCanonicalName(),
                                "print", Type.VOID, new Type[]{new ObjectType(String.class.getCanonicalName())}, Constants.INVOKEVIRTUAL));

                        returnInstr.append(InstructionFactory.createDup(returnType.getSize()));
                        returnInstr.append(instructionFactory.createFieldAccess(
                                System.class.getCanonicalName(), "out", new ObjectType(PrintStream.class.getCanonicalName()), Constants.GETSTATIC));

                        if (returnType.equals(Type.DOUBLE) || returnType.equals(Type.LONG)) {
                            returnInstr.append(InstructionFactory.DUP_X2);
                            returnInstr.append(InstructionFactory.POP);
                        } else {
                            returnInstr.append(InstructionFactory.SWAP);
                        }

                        if(returnType instanceof BasicType) {
                            returnInstr.append(instructionFactory.createInvoke(
                                    String.class.getCanonicalName(), "valueOf", Type.STRING, new Type[]
                                            { returnType == Type.BYTE || returnType == Type.SHORT ? Type.INT : returnType },
                                    Constants.INVOKESTATIC));
                        } else {
                            returnInstr.append(instructionFactory.createInvoke(
                                    String.class.getCanonicalName(), "valueOf", Type.STRING, new Type[] { Type.OBJECT }, Constants.INVOKESTATIC));
                        }

                        returnInstr.append(instructionFactory.createInvoke(PrintStream.class.getCanonicalName(),
                                "println", Type.VOID, new Type[] { Type.STRING }, Constants.INVOKEVIRTUAL));

                        instructionList.append(instructionHandle, returnInstr);
                    }
                }
            }
        }

        methodGenerator.setMaxStack();
        classGen.replaceMethod(method, methodGenerator.getMethod());
    }
}
