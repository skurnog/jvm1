import java.awt.Dimension;

public class Test2 {

    public int xD = 7;

    public void a() {
    }

    public void b() {
        a();
    }

    public int c() {
        a();
        return 3;
    }

    public String d() {
        a();
        return "d";
    }

    public long e(int cc) {
        a();
        return 10*cc;
    }

    public Long f() {
        return 10L;
    }

    public char[] g() {
        return "Anatomia".toCharArray();
    }

    public short h() {
        return 10;
    }

    public int i() {
        return (new Dimension(10, 20)).height;
    }

    public byte j() {
        return (byte) (new Dimension(10, 20)).getHeight();
    }

    public static void main(String[] args) {
        Test2 t = new Test2();

        t.a();
        t.b();
        t.c();
        t.d();
        t.e(6);
        t.f();
        t.g();
        t.h();
        t.i();
        t.j();

        try {
            t.getClass().getField("xD");
        } catch (Exception e){

        }
    }

}
