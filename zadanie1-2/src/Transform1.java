import com.sun.org.apache.bcel.internal.Constants;
import org.apache.bcel.Repository;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.*;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;

public class Transform1 {

    private static ClassGen classGen;
    private static ConstantPoolGen constantPoolGen;

    public static void main(String[] args) throws ClassNotFoundException, IOException {

        JavaClass clazz = Repository.lookupClass(args[0]);
        classGen = new ClassGen(clazz);
        constantPoolGen = classGen.getConstantPool();

        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            transform(method);
        }

        classGen.getJavaClass().dump(Repository.lookupClassFile(classGen.getClassName()).getPath() + "kk.class");
    }

    private static void transform(Method method) {
        MethodGen methodGen = new MethodGen(method, classGen.getClassName(), constantPoolGen); //create new method generator
        InstructionList instructionList = methodGen.getInstructionList();
        InstructionFactory instructionFactory = new InstructionFactory(classGen);

        Iterator<InstructionHandle> iterator = instructionList.iterator();
        while (iterator.hasNext()) {
            InstructionHandle instruction = iterator.next();

            if (instruction.getInstruction() instanceof GETFIELD) {
                Type fieldType = ((GETFIELD) instruction.getInstruction()).getType(constantPoolGen);

                if(fieldType instanceof BasicType) {
                    GETFIELD field = (GETFIELD) instruction.getInstruction();

                    InstructionList newInstr = new InstructionList();

                    StringBuilder builder = new StringBuilder();
                    builder.append("Before getfield:\n");

                    builder.append("    ");
                    builder.append(field.getReferenceType(constantPoolGen));
                    builder.append("\n");

                    builder.append("    ");
                    builder.append(fieldType.toString());
                    builder.append("\n");

                    builder.append("    ");
                    builder.append(field.getFieldName(constantPoolGen));
                    builder.append("\n");

                    // print first part of message
                    newInstr.append(instructionFactory.createPrintln(builder.toString()));

                    // print 4 spaces
                    newInstr.append(instructionFactory.createFieldAccess(System.class.getCanonicalName(), "out", new ObjectType(PrintStream.class.getCanonicalName()), Constants.GETSTATIC));
                    newInstr.append(instructionFactory.createConstant("    "));
                    newInstr.append(instructionFactory.createInvoke(PrintStream.class.getCanonicalName(), "print", Type.VOID, new Type[]{Type.STRING}, Constants.INVOKEVIRTUAL));

                    // duplicate value on stack to do with it whatever...
                    if(fieldType == Type.DOUBLE || fieldType == Type.LONG){
                        newInstr.append(InstructionFactory.DUP2);
                    } else {
                        newInstr.append(InstructionFactory.DUP);
                    }

                    newInstr.append(instructionFactory.createFieldAccess(System.class.getCanonicalName(), "out", new ObjectType(PrintStream.class.getCanonicalName()), Constants.GETSTATIC));
                    newInstr.append(InstructionFactory.SWAP);

                    newInstr.append((instructionFactory.createInvoke(String.class.getCanonicalName(), "valueOf", Type.STRING, new Type[] {fieldType == Type.BYTE || fieldType == Type.SHORT ? Type.INT : fieldType}, Constants.INVOKESTATIC)));
                    newInstr.append(instructionFactory.createInvoke(PrintStream.class.getCanonicalName(), "println", Type.VOID, new Type[] { Type.STRING }, Constants.INVOKEVIRTUAL));

                    // part 2
                    newInstr.append(field);
                    if(fieldType == Type.DOUBLE || fieldType == Type.FLOAT || fieldType == Type.LONG || fieldType == Type.INT || fieldType == Type.SHORT || fieldType == Type.BYTE){
                        if(fieldType == Type.DOUBLE || fieldType == Type.LONG){
                            newInstr.append(InstructionFactory.DUP2);
                        } else {
                            newInstr.append(InstructionFactory.DUP);
                        }

                        newInstr.append(field);

                        if(fieldType == Type.DOUBLE){
                            newInstr.append(new D2I());
                        } else if (fieldType == Type.LONG){
                            newInstr.append(new L2I());
                        } else if (fieldType == Type.FLOAT){
                            newInstr.append(new F2I());
                        }

                        newInstr.append(new BIPUSH((byte) 30));

                        BranchHandle branchHandle = newInstr.append(new IF_ICMPLT(null));
                        newInstr.append(instructionFactory.createFieldAccess(System.class.getCanonicalName(), "out", new ObjectType(PrintStream.class.getCanonicalName()), Constants.GETSTATIC));
                        newInstr.append(instructionFactory.createConstant("    !the value is greater than 30!"));
                        newInstr.append(instructionFactory.createInvoke(PrintStream.class.getCanonicalName(), "println", Type.VOID, new Type[]{Type.STRING}, Constants.INVOKEVIRTUAL));
                        InstructionHandle handle = newInstr.append(new NOP());
                        branchHandle.setTarget(handle);
                    }

                    instructionList.insert(field,  newInstr);
                }
            }
        }

        methodGen.setInstructionList(instructionList);
        classGen.replaceMethod(method, methodGen.getMethod());
    }
}
